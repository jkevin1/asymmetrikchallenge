#pragma once

#include <stdint.h>
#include <vector>

namespace Utilities
{
	// Loads an entire file into the specified buffer
	bool ReadCompleteFile(const char* filename, std::vector<char>* buffer);

	// Returns milliseconds since program execution began
	float GetCurrentTimeMS();

	// Allocates blocks of N instances of type T
	// Amortizes cost of allocating memory
	template<class T, uint32_t N>
	struct BlockAllocator
	{
		~BlockAllocator()
		{
			FreeAll();
		}

		T* Alloc()
		{
			if (currentBlock == nullptr || blockUsage >= N)
			{
				currentBlock = new T[N];
				blocks.push_back(currentBlock);
				blockUsage = 0;
			}

			return &currentBlock[blockUsage++];
		}

		void FreeAll()
		{
			for (T* block : blocks)
			{
				delete[] block;
			}
			blocks.clear();
		}

	private:
		std::vector<T*> blocks;
		T* currentBlock = nullptr;
		uint32_t blockUsage = 0;
	};

}