#include "Trie.h"
#include <ctype.h> // tolower, isalpha
#include <string>

static uint32_t nBytesAllocated = 0;

//==================================================================================================
//==================================================================================================
static void ClearNode(Trie::Node* pNode)
{
	memset(pNode, 0, sizeof(Trie::Node));
}

//==================================================================================================
//==================================================================================================
template<typename Callback>
static void TraverseChildren(Trie::Node* pNode, Callback kCallback, std::string* pBuffer)
{
	char testChar = 'a';
	for (Trie::Node* child : pNode->children)
	{
		pBuffer->push_back(testChar);

		if (child != nullptr)
		{
			if (child->counter > 0)
			{
				kCallback(pBuffer->c_str(), child->counter);
			}

			TraverseChildren(child, kCallback, pBuffer);
		}

		pBuffer->pop_back();
		testChar++;
	}
}

//==================================================================================================
//==================================================================================================
Trie::Trie()
{
	
}

//==================================================================================================
//==================================================================================================
Trie::~Trie()
{
	allocator.FreeAll();
}

//==================================================================================================
//==================================================================================================
void Trie::Train(const char* start, uint32_t count)
{
	Node* node = &root;
	for (uint32_t i = 0; i < count; i++)
	{
		char nextChar = start[i];
		if (nextChar >= 0 && isalpha(nextChar))
		{
			nextChar = tolower(nextChar);
			uint32_t letterIndex = nextChar - 'a';
			if (node->children[letterIndex] == nullptr)
			{
				// allocate a new node for the Trie
				node->children[letterIndex] = allocator.Alloc();
				ClearNode(node->children[letterIndex]);
				nBytesAllocated += sizeof(Node);
			}

			node = node->children[letterIndex];
		}
		else
		{
			// increment the counters
			if (node != &root)
			{
				nTotalWords++;
				node->counter++;
				if (node->counter == 1) nUniqueWords++;
			}

			// reset loop
			node = &root;
		}
	}

	// If we have reached the end, add the last word
	if (node != &root)
	{
		nTotalWords++;
		node->counter++;
		if (node->counter == 1) nUniqueWords++;
	}
}

//==================================================================================================
//==================================================================================================
void Trie::ForEachMatch(const char* key, uint32_t length, const std::function<void(const char*, int)>& callback)
{
	Node* node = &root;
	for (uint32_t i = 0; i < length; i++)
	{
		char nextChar = tolower(key[i]);
		if (isalpha(nextChar))
		{
			uint32_t letterIndex = nextChar - 'a';
			if (node->children[letterIndex] == nullptr)
			{
				fprintf(stdout, "No matches for key: '%s'\n", key);
				return;
			}

			node = node->children[letterIndex];
		}
		else
		{
			fprintf(stderr, "Only letters are supported, found ASCII character [%i] in key '%s'\n", nextChar, key);
			return;
		}
	}

	std::string kCurrentWord = key;
	kCurrentWord.reserve(24);
	TraverseChildren(node, callback, &kCurrentWord);
}