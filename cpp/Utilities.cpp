#include "Utilities.h"
#include <stdio.h>
#include <chrono>

// windows complains about using fopen
#ifdef _WIN32
#pragma warning(disable: 4996)
#endif

bool Utilities::ReadCompleteFile(const char* filename, std::vector<char>* buffer)
{
	FILE* file = fopen(filename, "rb");

	if (file != NULL)
	{
		fseek(file, 0, SEEK_END);
		long fileSize = ftell(file);
		fseek(file, 0, SEEK_SET);

		// resize the buffer so it can fit the whole file plus a null terminator
		buffer->resize(fileSize + 1);
		char* pointer = buffer->data();

		// fill the buffer
		fread(pointer, fileSize, 1, file);
		pointer[fileSize] = '\0';

		fclose(file);
		return true;
	}

	return false;
}

static const auto start = std::chrono::high_resolution_clock::now();
static const float NS_PER_MS = 1000.0f * 1000.0f;

float Utilities::GetCurrentTimeMS()
{
	using Nanoseconds = std::chrono::nanoseconds;
	auto time = std::chrono::high_resolution_clock::now();
	auto nanos = std::chrono::duration_cast<Nanoseconds>(time - start);
	return nanos.count() / NS_PER_MS;
}