C++ does not have consistent compilation tools across platform. Windows has MSVC and MinGW, Linux has clang and gcc, MacOS has clang and XCode, plus a couple more special build tools.
I have included a cmake file to help build the project on whatever platform you may be testing on.

Note: I have not tested compilation on platforms other than windows. If it fails to compile, send me the compiler output and I will try to address any compilation errors.

Install cmake (at least version 2.8.9)
Optionally create a build directory
Use cmake command to generate a project for your platform
Use your platform-specific IDE or build tool to compile the project

Windows example, starting in this directory
1. mkdir build
2. cd build
3. cmake -g "Visual Studio 15 2017 Win64" ..
4. open 'AutoComplete.sln' in Visual Studio
5. compile and run using Visual Studio

Generic unix example, starting in this directory
1. mkdir build
2. cd build
3. cmake -g "Unix Makefiles" ..
4. make -f "path to makefile"
5. run resulting executable