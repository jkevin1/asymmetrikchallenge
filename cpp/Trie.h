#pragma once

#include "Utilities.h"
#include <vector>
#include <functional>

// A specialized Trie or Prefix Tree that increments instances of words
// The words themselves are rebuilt from traversing the data structure instead of being stored at each node to reduce memory
class Trie
{
public:
	Trie();
	~Trie();

	void Train(const char* start, uint32_t count);

	// adds all results that match the key to the results vector
	//void Find(const char* key, uint32_t length, std::vector<Result>* results);
	void ForEachMatch(const char* key, uint32_t length, const std::function<void(const char*, int)>& callback);

//private:
	struct Node
	{
		int counter = 0;                  // number of instances of the current word aka "confidence"
		Node* children[26] = { nullptr }; // pointers to child nodes sorted by letter
	};

	Node root;
	uint32_t nUniqueWords = 0;
	uint32_t nTotalWords = 0;

	Utilities::BlockAllocator<Node, 64> allocator;
};