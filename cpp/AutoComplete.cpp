#include "Trie.h"

#include "Utilities.h"
#include <stdio.h>

#ifdef _WIN32
#include <Windows.h>
#endif


struct Candidate
{
public:
	// constructor
	Candidate(const char* s, int c) : word(s), confidence(c) { }

	// returns the autocomplete candidate
	const char* getWord() const { return word.c_str(); }

	// returns the confidence* for the candidate
	int getConfidence() const { return confidence; };

private:
	std::string word;
	int confidence;
};

class AutocompleteProvider
{
public:
	// returns list of candidates ordered by confidence*
	std::vector<Candidate> getWords(const char* fragment)
	{
		std::vector<Candidate> results;
		results.reserve(32);

		auto callback = [&results](const char* word, int confidence)
		{
			results.emplace_back(word, confidence);
		};
		trie.ForEachMatch(fragment, (uint32_t)strlen(fragment), callback);

		return results;
	}

	// trains the algorithm with the provided passage
	void train(const char* passage, uint32_t length)
	{
		trie.Train(passage, length);
	}

private:
	Trie trie;
};

static void printCandidates(const std::vector<Candidate>& results)
{
	for (const auto& result : results)
	{
		fprintf(stdout, "  '%s' (confidence = %d)\n", result.getWord(), result.getConfidence());
	}
}

int main(int argc, char* argv[])
{
	AutocompleteProvider autocomplete;

	if (argc < 2)
	{
		printf("Usage: autocomplete <file> <fragment1> <fragment2> ...\n");
		return 0;
	}

	std::vector<char> buffer;
	Utilities::ReadCompleteFile(argv[1], &buffer);

	float start = Utilities::GetCurrentTimeMS();
	autocomplete.train(buffer.data(), (uint32_t)buffer.size());
	float end = Utilities::GetCurrentTimeMS();
	printf("Training took %fms\n", end - start);

	for (int i = 2; i < argc; i++)
	{
		start = Utilities::GetCurrentTimeMS();
		auto results = autocomplete.getWords(argv[i]);
		end = Utilities::GetCurrentTimeMS();
		printf("getWords(%s) took %fms\n", argv[i], end - start);

		printCandidates(results);
	}

#ifdef _WIN32
	// Stop the console window from closing
	system("pause");
#endif
}