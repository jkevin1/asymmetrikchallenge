import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;

class Candidate {
	
	public Candidate(String word, int confidence)
	{
		this.word = word;
		this.confidence = confidence;
	}

    String getWord() { return word; }              // returns the autocomplete candidate
    Integer getConfidence() { return confidence; } // returns the confidence* for the candidate

    String word;
    int confidence;
}

// This is basically a specialized Trie which counts words
public class AutoCompleteProvider {

	private class Node {
		int count = 0;
		HashMap<Character, Node> children = new HashMap<Character, Node>();
	}
	
	Node root = new Node();
	int totalWords = 0;
	int uniqueWords = 0;

	// trains the algorithm with the provided passage
	public void train(String passage) {
		
		Node currentNode = root;
		
		int length = passage.length();
		for (int i = 0; i < length; i++) {
			char nextChar = passage.charAt(i);
			
			if (nextChar >= 0 && Character.isAlphabetic(nextChar)) {
				nextChar = Character.toLowerCase(nextChar);
				if (!currentNode.children.containsKey(nextChar)) {
					// create a new node for the Trie
					currentNode.children.put(nextChar, new Node());
				}

				currentNode = currentNode.children.get(nextChar);
			} else {
				// increment the counters
				if (currentNode != root) {
					totalWords++;
					currentNode.count++;
					if (currentNode.count == 1) uniqueWords++;
				}

				// reset loop
				currentNode = root;
			}
		}

		// If we have reached the end, add the last word
		if (currentNode != root) {
			totalWords++;
			currentNode.count++;
			if (currentNode.count == 1) uniqueWords++;
		}
		
	}

	// returns list of candidates ordered by confidence
	List<Candidate> getWords(String fragment) { 
		
		Node currentNode = root;
		
		List<Candidate> results = new ArrayList<Candidate>();
		
		final int length = fragment.length();
		for (int i = 0; i < length; i++) {
			char nextChar = Character.toLowerCase(fragment.charAt(i));
			if (Character.isAlphabetic(nextChar)) {
				currentNode = currentNode.children.get(nextChar);
				if (currentNode == null) return results;
			} else {
				throw new IllegalArgumentException("Only letters are supported, found ASCII character '" + nextChar + "' in key '" + fragment + "'");
			}
		}

		BiConsumer<String, Integer> callback = (word, confidence) -> { results.add(new Candidate(word, confidence)); };
		TraverseChildren(currentNode, callback, fragment);
		
		return results;
	}

	private static void TraverseChildren(Node node, BiConsumer<String, Integer> callback, String parent) {
		node.children.forEach((letter, child) -> {
			if (child != null) {
				String word = parent + letter.charValue();
				
				if (child.count > 0) { callback.accept(word, child.count); }
	
				TraverseChildren(child, callback, word);
			}
		});
	}
	
	public static void main(String[] args) {
		
		AutoCompleteProvider autocomplete = new AutoCompleteProvider();
		
		if (args.length < 1) {
			System.out.println("Usage: autocomplete <file> <fragment1> <fragment2> ...");
			return;
		}
		
		// load the file
		String text = "";
		try {
			File file = new File(args[0]);
			byte[] fileContent = Files.readAllBytes(file.toPath());
			text = new String(fileContent);
		} catch (IOException e) {
			System.err.format("Failed to open file %s\n", args[0]);
			return;
		}

		long start = System.currentTimeMillis();
		autocomplete.train(text);
		long elapsed = System.currentTimeMillis() - start;
		System.out.format("Training took %dms\n", elapsed);

		for (int i = 1; i < args.length; i++) {
			start = System.currentTimeMillis();
			List<Candidate> results = autocomplete.getWords(args[i]);
			elapsed = System.currentTimeMillis() - start;
			System.out.format("getWords(%s) took %dms\n", args[i], elapsed);
			
			for (Candidate result : results) {
				System.out.format("  '%s' (confidence = %d)\n", result.getWord(), result.getConfidence());
			}
		}
	}
	
}
